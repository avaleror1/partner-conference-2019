# README

This content was presented in the lab "Enhance the Power of Cloudforms with the inbuilt Ansible Features" at the Red Hat Partner Conference 2019 in Prague.

The lab is available at:

[Enhance the Power of Cloudforms with the inbuilt Ansible Features](./cloudforms-and-ansible-deep-dive/index.md)

If you have questions or found a bug, please open an issue in the repository.
